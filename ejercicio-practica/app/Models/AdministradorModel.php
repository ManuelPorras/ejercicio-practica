<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AdministradorModel extends Model
{
    use HasFactory;
    public static function guardar($arrayData) {

        /*echo "<PRE>";
            print_r($arrayData);
        echo "</PRE>";*/
    
        $fechaRegistro=date("Y-m-d H:i:s");
                          
        $arrayDetalle = json_decode($arrayData['datos']);
       /* echo "<PRE>";
            print_r($arrayData);
        echo "</PRE>";*/
        foreach($arrayDetalle as $cl=>$vl){
            if($vl!=""){
                $nombre=$vl[0];
                $rfc=$vl[1];
                $correo=substr($vl[2], 0, -1);
                $dataU[]= ['glu_nombre'=>$nombre, 'glu_fecha_registro'=>$fechaRegistro, 'glu_tipo'=>'P'];
                $dataP[]= ['pro_rfc'=>$rfc, 'pro_email'=>$correo];


            }            
        }    
        $result=DB::table('gl_usuario')->insert($dataU);
        $result= DB::table('cm_proveedor')->insert($dataP);
        return $result;
    }
}
