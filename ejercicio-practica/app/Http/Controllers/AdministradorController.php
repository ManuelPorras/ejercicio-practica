<?php

namespace App\Http\Controllers;

use App\Models\AdministradorModel;
use Illuminate\Http\Request;
use Exception;
use GrahamCampbell\ResultType\Result;
use Illuminate\Support\Facades\DB;


class AdministradorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('Administrador.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $msg="Exito";
            $result=1;
            DB::beginTransaction();
           $result=AdministradorModel::guardar($request->all());
          
                DB::commit();
                
        } catch (Exception $ex) {
            DB::rollBack();
            /*echo "<PRE>";
            dd($ex->getMessage());//pinta el resultado
            echo "</PRE>";*/
            $result=0;
            $msg=$ex->getMessage();
        }
        
        
        return ["result"=>$result,"msg"=>$msg];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
