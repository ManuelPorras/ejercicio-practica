@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Bienvenido') }} {{ Auth::user()->name }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        <div class="row">
                            <div class="col-lg-12">
                                
                    {{ __('Estas logueado Como administrador!') }}
                            </div>
                        </div>
                    
                    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
                    <script type="text/javascript">
                    $(document).ready(function(){
                        valueToPush = [];
                        $("#fileUpload").val('');
                        jQuery("#btnAceptar").prop('disabled',true);
                            $("#fileUpload").change(function(){
                                var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv|.txt)$/;
                                if (regex.test($("#fileUpload").val().toLowerCase())) {
                                    if (typeof (FileReader) != "undefined") {
                                        var reader = new FileReader();
                                        reader.onload = function (e) {
                                            var table = $("<table />");
                                            var rows = e.target.result.split("\n");
                                            for (var i = 0; i < rows.length; i++) {
                                                var row = $("<tr />");
                                                var cells = rows[i].split(",");
                                                console.log(cells[0]);
                                                if(i>0){
                                                    if(cells.length >1){

                                                        valueToPush.push(cells);
                                                    }
                                                }
                                                if (cells.length > 1) {
                                                    for (var j = 0; j < cells.length; j++) {
                                                        var cell = $("<td />");
                                                        cell.html(cells[j]);
                                                        row.append(cell);
                                                    }
                                                    table.append(row);
                                                }
                                            }
                                            $("#dvCSV").html('');
                                            $("#dvCSV").append(table);
                                            console.log(valueToPush);
                                            if(valueToPush.length > 0 ){
                                                jQuery("#btnAceptar").prop('disabled',false);
                                            }else{
                                                jQuery("#btnAceptar").prop('disabled',true);
                                            }
                                        }
                                        reader.readAsText($("#fileUpload")[0].files[0]);
                                    } else {
                                        alert("This browser does not support HTML5.");
                                    }
                                } else {
                                    alert("Please upload a valid CSV file.");
                                }
                            });

                            jQuery("#btnAceptar").click(function() {

                            //console.log("TOTAL");
                            //console.log(valueToPush.length);
                            if(valueToPush.length==0){
                                showMessage('error',"no existen datos para registrar");
                                return false;
                            }

                            jQuery("#"+jQuery("form").attr('id')).attr('action',localStorage.getItem("route"));
                            save("#"+jQuery("form").attr('id'),'#divMsj',valueToPush);
                            return false;
                            });	
                            jQuery("#btnRechazar").click(function() {
                                $("#dvCSV").html('');
                                
                                jQuery("#btnAceptar").prop('disabled',false);
                            return false;
                            });	
                            

                            function setRoute(route){
                                localStorage.setItem("route", route);
                            }

                            setRoute('/administrador');

                            var clase="";
                            function save(idForm,divMjs,valueToPush){
                                jQuery.ajax({
                                    type: jQuery(idForm).attr('method'),
                                    url: jQuery(idForm).attr('action'),                      
                                    data: jQuery(idForm).serialize()+"&datos="+JSON.stringify(valueToPush),
                                    //dataType: "json",
                                    dataType: "text",
                                    success: function (response) {
                                       // console.log(response);
                                        //console.log(response['result']);
                                        var obj = JSON.parse(response); 
                                        console.log(obj);
                                        $(divMjs).removeClass('alert alert-info');
                                        $(divMjs).addClass(clase);		
                                        $(divMjs).html(obj.msg);
                                        alert("Registro almacenado correctamente");
                                        jQuery("#btnAceptar").prop('disabled',true);
                                        
                                    },
                                    beforeSend: function () {
                                        $(divMjs).addClass('alert alert-info');
                                        $(divMjs).html("Guardando Datos.....");
                                    }
                                });
                            }
                        });
                    </script>
                    <input type="file" id="fileUpload" />
                    <form id="frmSave" name="frmSave" method="post">
                        <div id="divMjs"></div>
                        <input type="button" id="btnAceptar" name="fileUpload" class="btn btn-success btn-sm" value="Aceptar" disabled />

                        <input type="button" id="btnRechazar" name="btnRechazar" class="btn btn-danger btn-sm" value="Rechazar" />
                    </form>
                    <hr />
                    <div id="dvCSV">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
