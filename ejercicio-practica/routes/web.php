<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/proveedor', [App\Http\Controllers\ProveedorController::class, 'index'])->name('proveedor');
Route::get('/administrador', [App\Http\Controllers\AdministradorController::class, 'index'])->name('administrador');
Route::post('administrador', [App\Http\Controllers\AdministradorController::class, 'store'])->name('administrador.store');
